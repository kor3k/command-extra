<?php

/*
 * This file is part of the portaldrazeb.cz package.
 *
 * (c) Exekutorská Komora České republiky
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use FOS\ElasticaBundle\Command\PopulateCommand as ElasticaCommand;
use Snc\RedisBundle\Command\RedisFlushallCommand as RedisCommand;
use Symfony\Component\Messenger\Command\SetupTransportsCommand as MessengerCommand;

/**
 * This class contains app:auction:join command.
 */
class DatabaseSetupCommand extends AbstractCommand
{
    /**
     * Command name.
     */
    public const COMMAND_NAME = 'app:database:setup';
    public const COMMAND_DESC = 'Purge all data from database. Optionally load fixtures.';

    protected array $sql = [];

    public function __construct(string ...$sql)
    {
        parent::__construct();
        $this->sql = $sql;
    }


    protected function configure(): void
    {
        parent::configure();

        if(class_exists(ElasticaCommand::class)) {
            $this->addOption('populate-elastic', 'E', InputOption::VALUE_OPTIONAL, 'Populate elastic after purge', false);
        }

        if(class_exists(RedisCommand::class)) {
            $this->addOption('purge-redis', 'R', InputOption::VALUE_OPTIONAL, 'Also purge redis', false);
        }

        if(class_exists(MessengerCommand::class)) {
            $this->addOption('setup-messenger', 'M', InputOption::VALUE_OPTIONAL, 'Setup messenger after purge', false);
        }

        $this->addOption('load-fixtures', 'L', InputOption::VALUE_OPTIONAL, 'Load fixtures after purge', false);
        $this->addOption('load-sql', 'S', InputOption::VALUE_OPTIONAL, 'Load sql after purge', false);
        $this->addArgument('sql-files', InputArgument::OPTIONAL|InputArgument::IS_ARRAY, 'Override SQL files for load-sql');
        $this->addOption('purge-cache', 'C', InputOption::VALUE_OPTIONAL, 'Purge app cache', false);
        $this->addOption('force', 'f', InputOption::VALUE_OPTIONAL, 'Force run in production', false);
    }

    /**
     * {@inheritdoc}
     */
    protected function doExecute(InputInterface $input, ConsoleOutputInterface $output, SymfonyStyle $ui)
    {
        if (!in_array($_ENV['APP_ENV'], ['dev', 'test']) && !$input->getOption('force')) {
            $ui->error('This command must not be run in the production environment!');
            return 1;
        }

        if (!$ui->confirm(sprintf('Careful, database will be purged. Do you want to continue?'), !$input->isInteractive())) {
            $this->writelnTs('Database unchanged', $ui);
            return 0;
        }

        if($input->getOption('purge-cache')) {
            $this->writelnTs('Clearing cache pools', $ui);
            $this->runAnotherCommand($output, 'cache:pool:clear', ['pools' => ['cache.app_clearer']]);
        }

        $this->writelnTs('Dropping schema', $ui);
        $this->runAnotherCommand($output, 'doctrine:schema:drop', ['--force' => true]);

        if (class_exists(RedisCommand::class) && $input->getOption('purge-redis')) {
            $this->writelnTs('Purging redis', $ui);
            $this->runAnotherCommand($output, 'redis:flushall', []);
        }

        $this->writelnTs('Updating schema', $ui);
        $this->runAnotherCommand($output, 'doctrine:schema:update', ['--force' => true]);

        if ($input->getOption('load-sql')) {
            $this->writelnTs('Loading sql', $ui);
            $this->loadSql($input, $output, $ui);
        }

        if ($input->getOption('load-fixtures')) {
            $this->writelnTs('Loading fixtures', $ui);
            $this->loadFixtures($input, $output, $ui);
        }

        if (class_exists(MessengerCommand::class) && $input->getOption('setup-messenger')) {
            $this->writelnTs('Setting up messenger', $ui);
            $this->runAnotherCommand($output, 'messenger:setup-transports', []);
        }

        if (class_exists(ElasticaCommand::class) && $input->getOption('populate-elastic')) {
            $this->writelnTs('Populating elasticsearch', $ui);
            $this->runAnotherCommand($output, 'fos:elastica:populate', []);
        }

        $this->writelnTs('Database purged', $ui);

        return 0;
    }

    protected function loadSql(InputInterface $input, ConsoleOutputInterface $output, SymfonyStyle $ui)
    {
        $sql = $this->sql;

        if(!empty($input->getArgument('sql-files'))) {
            $sql = $input->getArgument('sql-files');
        }

        foreach($sql as $file) {
            $this->runAnotherCommand($output, 'doctrine:database:import', ['file' => $file]);
        }
    }

    protected function loadFixtures(InputInterface $input, ConsoleOutputInterface $output, SymfonyStyle $ui)
    {
        $this->runAnotherCommand($output, 'doctrine:fixtures:load', ['--append' => true]);
    }
}
