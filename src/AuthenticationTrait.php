<?php

/*
 * This file is part of the portaldrazeb.cz package.
 *
 * (c) Exekutorská Komora České republiky
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\Command;

use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

trait AuthenticationTrait
{
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    protected function authenticate(
        array $roles,
        $username = 'cli')
    {
        $this->tokenStorage->setToken(new PreAuthenticatedToken(
            $username,
            null,
            'command',
            $roles));
    }
}
