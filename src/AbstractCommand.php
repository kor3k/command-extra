<?php

/*
 * This file is part of the portaldrazeb.cz package.
 *
 * (c) Exekutorská Komora České republiky
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\ConsoleSectionOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * This class provides access to container and also some other useful command functions.
 */
abstract class AbstractCommand extends Command
{
    public const COMMAND_NAME = 'app:some:command';
    public const COMMAND_DESC = 'AbstractCommand description';

    /**
     * Format bytes to to human readable format.
     *
     * @param $bytes
     * @param string $force_unit
     * @param null   $format
     * @param bool   $si         If true, bytes will be power of 1000 instead of 10214 (SI decimal instead of binary)
     *
     * @return string
     */
    public static function bytes($bytes, $force_unit = 'B', $format = null, $si = true)
    {
        // Format string
        $format = (null === $format) ? '%01.2f %s' : (string) $format;

        // IEC prefixes (binary)
        if (false === $si || false !== strpos($force_unit, 'i')) {
            $units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];
            $mod = 1024;
        }
        // SI prefixes (decimal)
        else {
            $units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB'];
            $mod = 1000;
        }

        // Determine unit to use
        if (false === ($power = array_search((string) $force_unit, $units))) {
            $power = ($bytes > 0) ? floor(log($bytes, $mod)) : 0;
        }

        return sprintf($format, $bytes / pow($mod, $power), $units[$power]);
    }

    /**
     * This method configures command.
     *
     * This command *must* always be called in every inherited command as parent::configure() method
     */
    protected function configure(): void
    {
        $this->setName(static::COMMAND_NAME);
        $this->setDescription(static::COMMAND_DESC);
    }

    /**
     * The command itself, which should be executed.
     *
     * @param InputInterface         $input
     * @param ConsoleOutputInterface $output
     * @param SymfonyStyle           $symfonyStyle
     *
     * @return mixed
     */
    abstract protected function doExecute(InputInterface $input, ConsoleOutputInterface $output, SymfonyStyle $symfonyStyle);

    /**
     * Write line with current time.
     *
     * @param string          $line
     * @param OutputInterface $output
     */
    protected function writelnTs(string $line, OutputInterface $output): void
    {
        $output->writeln(sprintf('[%s] %s', date('H:i:s'), $line));
    }

    /**
     * Execute command.
     *
     * @see AbstractCommand::doExecute() for the command body
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $symfonyStyle = new SymfonyStyle($input, $output);
        $this->writelnTs('<info>This might take a while ...</info>', $symfonyStyle);
        $result = $this->doExecute($input, $output, $symfonyStyle);

        if (0 === $result) {
            $symfonyStyle->success(sprintf('Command %s finished  with peak memory usage: %s', static::COMMAND_NAME, self::bytes(memory_get_peak_usage(), 'M')));

            return 0;
        }

        $symfonyStyle->error('Error occurred');
        $symfonyStyle->error($result);

        return $result;
    }

    /**
     * Creates table output with specified headers.
     *
     * @param ConsoleSectionOutput $section
     * @param array                $headers
     *
     * @return Table
     */
    protected function createTableOutput(ConsoleSectionOutput $section, array $headers)
    {
        $table = new Table($section);
        $table->setHeaders($headers);

        return $table;
    }

    /**
     * @param OutputInterface $output
     * @param string          $cmdName
     * @param array           $cmdArguments
     * @param bool            $isInteractive
     *
     * @throws \Exception
     *
     * @return int
     */
    protected function runAnotherCommand(
        OutputInterface $output,
        string $cmdName,
        array $cmdArguments = [],
        bool $isInteractive = false): int
    {
        $command = $this->getApplication()->find($cmdName);
        $arguments = array_merge(['command' => $cmdName], $cmdArguments);

        if ($isInteractive) {
            $input = new ArrayInput($arguments);
        } else {
            $arguments = array_merge(['--no-interaction' => true], $arguments);
            $input = new ArrayInput($arguments);
            $input->setInteractive(false);
        }

        return $command->run($input, $output);
    }
}
